<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>

<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase-storage.js"></script>

<script>
  // Initialize Firebase
  // TODO: Replace with your project's customized code snippet
  var config = {
    apiKey: "AIzaSyC84ZCm1RjRRy_VK7spL-31CRB8QEJtC78",
    authDomain: "<PROJECT_ID>.firebaseapp.com",
    databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
    projectId: "lara-art",
    storageBucket: "gs://lara-art.appspot.com/",
    messagingSenderId: "<SENDER_ID>",
  };
  
  firebase.initializeApp(config);
  var newPostRef = firebase.database().ref(); 
  

// setfileインスタンスを作成
const setfile = document.getElementById("FormControlFile_v");
// CloudStorageインスタンスを作成
const storage = firebase.storage();


// submitで処理開始
setfile.addEventListener('change', e => {
    var file = e.target.files;
    // fileの名前を取得
    file_name = file[0].name;
    // blob形式に変換
    blob = new Blob(file, { type: "video/mpeg" });

    // ページ遷移をなくす
    e.preventDefault();

    // storageのarea_imagesへの参照を定義
    var uploadRef = storage.ref('videos/').child(file_name);


    uploadRef.put(blob).then(snapshot => {
        console.log(snapshot.state);
        // アップロードしたファイルのurlを取得
        uploadRef.getDownloadURL().then(url => {

            g_url = url;
            console.log(g_url);
        }).catch(error => {
            console.log(error);
        });
    });
    // valueリセットする
    file_name = '';
    blob = '';
});
  
  
</script>