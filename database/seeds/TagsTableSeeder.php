<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  // 1レコード
  $tag = new \App\Tag([
    'name' => 'Violin',
  ]);
    $tag->save();

  $tag = new \App\Tag([
    'name' => 'Viola',
  ]);
    $tag->save();

  $tag = new \App\Tag([
    'name' => 'Cello',
  ]);
    $tag->save();

  $tag = new \App\Tag([
    'name' => 'Contrabass',
  ]);
    $tag->save();
    }
}
