<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1レコード
        $user = new \App\User([
              'id' =>'1',
              'name' => 'guest',
              'email'=>'guest@guest.com',
              'password'=>'qwertyuiopsdfghjkxcvbnm',
              'image_url'=>'BuwtwhjxO0OKj7Zsn7EM8aOWp59NqlHkqNyeUa4D.jpeg',
        ]);
    $user->save();
    }
}
