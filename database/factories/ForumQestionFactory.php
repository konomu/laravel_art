<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\ForumQuestion;
use Faker\Generator as Faker;

$factory->define(ForumQuestion::class, function (Faker $faker) {
    return [
        'title' => 'この弾き方がわかりません！',
        'question_comment' => ''
    ];
});
