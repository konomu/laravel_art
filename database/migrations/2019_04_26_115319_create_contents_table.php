<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('content_type')->nullable();
            //1 -> image
            //2 -> audio
            //3 -> video
            
            $table->integer('question_type')->nullable();
            // $table->integer('question_id')->nullable();
            $table->integer('forum_question_id')->nullable();
            $table->integer('answer_id')->nullable();
            $table->String('content_kind')->nullable();
            $table->String('content_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
