<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'ForumQuestionController@index')->name('top');

Route::resource('questions', 'ForumQuestionController', ['only' => ['create', 'store', 'show', 'edit', 'update', 'destroy']]);

Route::resource('answers', 'ForumAnswerController', ['only' => ['store', 'show', 'edit', 'update', 'destroy']]);

Route::post('/search', 'SearchController@index')->name('search');

Route::get('/myquestions','SearchController@myquestions')->name('myquestions');

Route::get('/myanswers','SearchController@myanswers')->name('myanswers');

Route::get('/myquestions_guest','SearchController@myquestions_guest')->name('myquestions_guest');

Route::get('/myanswers_guest','SearchController@myanswers_guest')->name('myanswers_guest');

Route::get('/tags','SearchController@tags')->name('tags');

Route::get('/tag_question/{id}','SearchController@tag_question')->name('tag_question');

Route::post('/best_answer','BestAnswer@post')->name('best');