<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumQuestion extends Model
{
    protected $fillable = ['title','question_comment','best_answer_id'];
    
    public function answers(){
        return $this->hasMany('App\ForumAnswer');
    }
    
    public function contents(){
        return $this->hasMany('App\Content');
    }
    
    public function user(){
        return $this->hasOne('App\User','id', 'user_id');
    }
    
    public function tagRelation(){
        return $this->hasMany('App\TagRelation','question_id', 'id');
    }
}
