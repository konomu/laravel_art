<?php

namespace App\Http\Controllers;

use App\ForumQuestion;
use App\ForumAnswer;
use App\Tag;
use App\TagRelation;
use Auth;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request){

        $keyword = $request->input('keyword');

        if(!empty($keyword)){
            $questions = ForumQuestion::where('question_comment', 'like' ,'%'.$keyword.'%')
                                      ->orderBy('created_at', 'desc')
                                      ->paginate(10);

            return view('search.index',[
                'questions' => $questions,
                'keyword' => $keyword,
            ]);
        }else{
            return redirect()->route('top');
        }
    }
        
    public function myquestions(){
        $user_id = auth()->user()->id;
        $questions = ForumQuestion::where('user_id', $user_id)
                                      ->orderBy('created_at', 'desc')
                                      ->paginate(10);

        $keyword = 'user->'.auth()->user()->name;

        return view('search.index',[
            'questions' => $questions,
            'keyword' => $keyword,
        ]);
    }
    
        public function myquestions_guest(){
        $user_id = 1;
        $questions = ForumQuestion::where('user_id', $user_id)
                                      ->orderBy('created_at', 'desc')
                                      ->paginate(10);

        $keyword = 'user->guest';

        return view('search.index',[
            'questions' => $questions,
            'keyword' => $keyword,
        ]);
    }
    
    
    
    public function myanswers(){
        $user_id = auth()->user()->id;

        $questions = ForumQuestion::whereIn('id', 
            function($query) use ($user_id) {
                $query->select('forum_question_id')
                      ->from(with(new ForumAnswer)->getTable())
                      ->where('user_id', $user_id);})
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $keyword = 'user->'.auth()->user()->name;

        return view('search.index',[
            'questions' => $questions,
            'keyword' => $keyword,
        ]);
    }
    
    public function myanswers_guest(){
        $user_id = 1;

        $questions = ForumQuestion::whereIn('id', 
            function($query) use ($user_id) {
                $query->select('forum_question_id')
                      ->from(with(new ForumAnswer)->getTable())
                      ->where('user_id', $user_id);})
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $keyword = 'user->guest';

        return view('search.index',[
            'questions' => $questions,
            'keyword' => $keyword,
        ]);
    }
    
    
    public function tags(){

        $tags = Tag::all();
        
        return view('search.tags',[
            'tags' => $tags,
        ]);
    }
    
    public function tag_question($id){
        
        $tag = Tag::find($id);
        // $tag = Tag::where('id', '=', $id);
        
        $questions = $tag->questions;
        // $tags->questions;
        
          return view('search.tag_question',[
            'tag'       => $tag,
            'questions' => $questions,
        ]);
    }
}
