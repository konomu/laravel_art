<?php

namespace App\Http\Controllers;

use App\ForumQuestion;
use App\ForumAnswer;
use Auth;

use Illuminate\Http\Request;

class ForumAnswerController extends Controller
{
    public function store(Request $request){
        
        $params = $request->validate([
            'question_id'     => 'required|exists:forum_questions,id',
            'answers_comment' => 'required|max:2000',
        ]);
        if (Auth::check()) {
        $params['user_id'] = auth()->user()->id;
        }else{
        $params['user_id'] = 1;    
        }
        $params['best_answer_flg'] = 0;
        $question = ForumQuestion::findOrFail($params['question_id']);
        $question->answers()->create($params);

        return redirect()->route('questions.show', ['question' => $question]);
    }
    
    public function edit($answer_id){
        $answer = ForumAnswer::findOrFail($answer_id);

        return view('answers.edit', [
            'answer' => $answer,
        ]);
    }
    
    public function update($answer_id, Request $request){
        $params = $request->validate([
            'answers_comment' => 'required|max:2000',
        ]);

        $answer = ForumAnswer::findOrFail($answer_id);
        $answer->fill($params)->save();
        $question = $answer->question;

        return view('questions.show', ['question' => $question]);
    }
    
    public function destroy($answer_id){
        $answer = ForumAnswer::findOrFail($answer_id);
        $question = $answer->question;
        $question->fill(['best_answer_id' => NULL])->save();
        
        \DB::transaction(function () use ($answer) {
            $answer->delete();
        });

        return view('questions.show', ['question' => $question]);
    }
}
