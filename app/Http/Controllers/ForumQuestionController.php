<?php

namespace App\Http\Controllers;

use App\ForumQuestion;
use App\ForumAnswer;
use App\Content;
use App\Tag;
use App\TagRelation;
use App\User;
use Auth;

use Illuminate\Http\Request;

class ForumQuestionController extends Controller
{
    public function index(){
        $questions = ForumQuestion::orderBy('created_at', 'desc')->paginate(10);

        return view('index', ['questions' => $questions]);
    }
    
    public function create(){
        $tags = Tag::all();
        
        return view('questions.create', ['tags' => $tags]);
    }

    public function store(Request $request){
        
        $params = $request->validate([
            'title'            => 'required|max:50',
            'question_comment' => 'required|max:2000',
            'tag_1'            => 'required',
        ]);
        $params = $request;
        
        //save(question)
        $question = new ForumQuestion;
        $question->title = $params->title;
        $question->question_comment = $params->question_comment;
        // $question->best_answer_id = 0;
        
        if (Auth::check()) {
            $question->user_id = auth()->user()->id;
        }else{
            $question->user_id = '1';
        }
        $question->save();

        //save(tag)
        $tag_relation = new TagRelation;
        $tag_relation->question_id   = $question->id;
        $tag_relation->tag_id        = $params->tag_1;
        $tag_relation->question_type = 0;
        $tag_relation->save();

        //save contents
        if($params->image_url != NULL){
            $content = new Content;
            $content->content_type = 1;//image
            $content->content_url = $params->image_url;
            $content->content_url = $request->file('image_url')->store('public/post_images');
            $content->content_url = str_replace('public/', '', $content->content_url);
            $question->contents()->save($content);
        }
        else if($params->audio_url != NULL){
            $content = new Content;
            $content->content_type = 2;//audio
            $content->content_url = $params->image_url;
            $content->content_url = $request->file('audio_url')->store('public/post_audios');
            $content->content_url = str_replace('public/', '', $content->content_url);
            $question->contents()->save($content);
        }
        elseif($params->video_url_uped != NULL){
            $content = new Content;
            $content->content_type = 3;//video
            $content->content_url = $params->video_url_uped;
            $question->contents()->save($content);
        }else{
            
        }
        
        return redirect()->route('top');
    }
    
    public function show($question_id){
        $question = ForumQuestion::findOrFail($question_id);
        return view('questions.show', [
            'question' => $question,
        ]);
    }
    
    public function edit($question_id){
        $question = ForumQuestion::findOrFail($question_id);

        return view('questions.edit', [
            'question' => $question,
        ]);
    }

    public function update($question_id, Request $request){
        $params = $request->validate([
            'title' => 'required|max:50',
            'question_comment' => 'required|max:2000',
        ]);

        $question = ForumQuestion::findOrFail($question_id);
        $question->fill($params)->save();

        return redirect()->route('questions.show', ['question' => $question]);
    }
    
    public function destroy($question_id){
        $question = ForumQuestion::findOrFail($question_id);

        \DB::transaction(function () use ($question) {
            $question->answers()->delete();
            $question->delete();
        });

        return redirect()->route('top');
    }
    
}
