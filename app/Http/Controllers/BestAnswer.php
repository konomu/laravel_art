<?php

namespace App\Http\Controllers;
use App\ForumQuestion;
use App\ForumAnswer;
use Illuminate\Http\Request;

class BestAnswer extends Controller
{
    public function post(Request $request){

        $answer = ForumAnswer::find($request->hidden_answer_id);
        $req_ans_id =  $request->hidden_answer_id;
        $question_id = $answer->question->id;
        
        $question = ForumQuestion::find($question_id);
        $best_ans_id = $question->best_answer_id;
        
        if(is_NULL($best_ans_id)){
            $question->fill(['best_answer_id' => $req_ans_id])->save();
            $answer->fill(['best_answer_flg' => 1])->save();
        }else{
            if($best_ans_id==$req_ans_id){
                $question->fill(['best_answer_id' => NULL])->save();
                $answer->fill(['best_answer_flg' => 0])->save();
            }else{
                $question->fill(['best_answer_id' => $req_ans_id])->save();
                
                $best_ans_old = ForumAnswer::find($best_ans_id);
                $best_ans_old->fill(['best_answer_flg' => 0])->save();
                
                $answer->fill(['best_answer_flg' => 1])->save();
            }
        }
        return view('questions.show', [
            'question' => $question,
        ]);
    }
}
