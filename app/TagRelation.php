<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagRelation extends Model
{
    protected $table = 'tag_relations';

    public function tag(){
        return $this->hasOne('App\Tag','id','tag_id');
    }




}
