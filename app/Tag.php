<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];
        
    public function questions(){
        // return $this->belongsToMany('App\ForumQuestion', 'tag_relations');
        return $this->belongsToMany('App\ForumQuestion', 'tag_relations', 'tag_id', 'question_id');        
        // return $this->belongsToMany('App\Tag', 'tag_relations', 'question_id','tag_id');
    }
}
