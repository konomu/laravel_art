<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumAnswer extends Model
{
    protected $fillable = ['title','answers_comment', 'user_id', 'best_answer_flg'];
    
    public function question(){
        return $this->belongsTo('App\ForumQuestion', 'forum_question_id');
    }
    public function user(){
        return $this->hasOne('App\User','id', 'user_id');
    }
}
