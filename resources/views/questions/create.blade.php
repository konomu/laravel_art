@extends('layouts/layout')

@section('content')

    <div class="container mt-4">
        <div class="border p-4">
            <h1 class="h5 mb-4">
                質問を投稿する
            </h1>
            <form method="POST" action="{{ route('questions.store') }}" enctype="multipart/form-data">
                @csrf
                <fieldset class="mb-4">
                    <div class="form-group">
                        <label for="title">
                            タイトル
                        </label>
                        <input
                            id="title"
                            name="title"
                            class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                            value="{{ old('title') }}"
                            type="text"
                        >
                        @if ($errors->has('title'))
                            <div class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="question_comment">
                            本文
                        </label>
                        <textarea
                            id="question_comment"
                            name="question_comment"
                            class="form-control {{ $errors->has('question_comment') ? 'is-invalid' : '' }}"
                            rows="4"
                        >{{ old('question_comment') }}</textarea>
                        @if ($errors->has('question_comment'))
                            <div class="invalid-feedback">
                                {{ $errors->first('question_comment') }}
                            </div>
                        @endif
                        <div class="form-group">
                          <label for="tag">タグ</label>
                          <select class="form-control" id="tag_1" name="tag_1">
                            <option value="">-</option>
                            @forelse ($tags as $tag)                            
                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                            @empty

                            @endforelse
                          </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="FormControlFile_i">画像ファイルを選択してください</label>
                            <input type="file" name=image_url class="form-control-file" id="FormControlFile_i">
                            <label for="FormControlFile_a">音声ファイルを選択してください</label>
                            <input type="file" name=audio_url class="form-control-file" id="FormControlFile_a">
                            <label for="FormControlFile_v">動画ファイルを選択してください</label>
                            <input type="file" name=video_url class="form-control-file" id="FormControlFile_v">
                            <input type="hidden" name=video_url_uped id="hidden_video_url">
                        </div>
                    
                    </div>
                    <div class="mt-5">
                        <a class="btn btn-secondary" href="{{ route('top') }}">
                            キャンセル
                        </a>

                        <button type="submit" class="btn btn-primary">
                            投稿する
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>


<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase.js"></script>

<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.9.1/firebase-storage.js"></script>

<script>
  // Initialize Firebase
  // TODO: Replace with your project's customized code snippet
  var config = {
    apiKey: "AIzaSyC84ZCm1RjRRy_VK7spL-31CRB8QEJtC78",
    authDomain: "<PROJECT_ID>.firebaseapp.com",
    databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
    projectId: "lara-art",
    storageBucket: "gs://lara-art.appspot.com/",
    messagingSenderId: "<SENDER_ID>",
  };
  
  firebase.initializeApp(config);
 

// setfileインスタンスを作成
const setfile = document.getElementById("FormControlFile_v");
// CloudStorageインスタンスを作成
const storage = firebase.storage();


// submitで処理開始
setfile.addEventListener('change', e => {
    var file = e.target.files;
    // fileの名前を取得
    file_name = file[0].name;
    // blob形式に変換
    blob = new Blob(file, { type: "video/mpeg" });

    // ページ遷移をなくす
    e.preventDefault();

    // storageのarea_imagesへの参照を定義
    var uploadRef = storage.ref('videos/').child(file_name);


    uploadRef.put(blob).then(snapshot => {
        console.log(snapshot.state);
        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        console.log('Upload is ' + progress + '% done');
        // アップロードしたファイルのurlを取得
        uploadRef.getDownloadURL().then(url => {

            // g_url = url;
            console.log(url);
            
            $('#hidden_video_url').val(url);
            
                        console.log($('#hidden_video_url').val());
            
            
            
            
            
            
            
        }).catch(error => {
            console.log(error);
        });
    });
    // valueリセットする
    file_name = '';
    blob = '';
});
  
  
</script>
@endsection