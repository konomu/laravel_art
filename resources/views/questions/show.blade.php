@extends('layouts/app')

@section('content')
    <div class="container mt-4">
        <div class="border p-4">
            <div class="mb-4 text-right">
                <a class="btn btn-primary" href="{{ route('questions.edit' , ['question' => $question])}}">
                編集
                </a>
                
                <form
                    style="display: inline-block;"
                    method="POST"
                    action="{{ route('questions.destroy', ['question' => $question]) }}"
                    >
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">削除</button>
                </form>
            </div>
            
            <img class="list-icon" height="70px" src="{{asset('storage/post_images/'.$question->user->image_url,true)}}"></img>
            <p>{{$question->user->name}}</p>
            
            <h1 class="h5 mb-4">
                {{ $question->title }}
            </h1>

            <p class="mb-5">
                {!! nl2br(e($question->question_comment)) !!}
            </p>
            
            @forelse ($question->tagRelation()->get() as $tag_relation)            
                <p class="mb-3">タグ：
                    <a href="{{url('tag_question',$tag_relation->tag->id)}}">
                    {{$tag_relation->tag->name}}
                    </a>
                </p>
            
            @empty
                {{--タグなし--}}
            @endforelse
            
            @forelse ($question->contents()->get() as $content)
                @if($content->content_type == 1) 
                    <img src="{{asset('storage/'.$content->content_url, true)}}" alt="no_image" class="img-thumbnail">
                @elseif($content->content_type == 2)
                    <audio src="{{asset('storage/'.$content->content_url, true)}}" controls>
                @elseif($content->content_type == 3)
                    <video src="{{$content->content_url}}" height="300px" controls type="video/mp3"></video>
                @endif
            @empty
                {{--コンテンツなし--}}
            @endforelse
            <form class="mb-4" method="POST" action="{{ route('answers.store') }}">
                @csrf
                <input
                    name="question_id"
                    type="hidden"
                    value="{{ $question->id }}"
                >
                <div class="form-group">
                    <label for="answers_comment">
                        本文
                    </label>
                    <textarea
                        id="answers_comment"
                        name="answers_comment"
                        class="form-control {{ $errors->has('answers_comment') ? 'is-invalid' : '' }}"
                        rows="4"
                    >{{ old('answers_comment') }}</textarea>
                    @if ($errors->has('answers_comment'))
                    <div class="invalid-feedback">
                        {{ $errors->first('answers_comment') }}
                    </div>
                    @endif
                </div>
                <div class="mt-4">
                    <button type="submit" class="btn btn-primary">
                        コメントする
                    </button>
                </div>
            </form>
            <section>
                <h2 class="h5 mb-4">
                    コメント
                </h2>
                @forelse($question->answers()->get() as $forum_answer)
                    <div class="border-top p-4">
                        <img class="list-icon" height="30px" src="{{asset('storage/post_images/'.$question->user->image_url,true)}}"></img>
                        <p>{{$forum_answer->user->name}}</p>
                        <time class="text-secondary">
                            {{ $forum_answer->created_at->format('Y.m.d H:i') }}
                        </time>
                        <p class="mt-2">
                            {!! nl2br(e($forum_answer->answers_comment)) !!}
                        </p>
                        
                        <form action="{{route('best')}}" method="POST">
                            @csrf
                            <!--<button class="btn btn-danger btn-sm">ベストアンサー</button>-->
                            <button class="best-ans-button">

                            @if($forum_answer->best_answer_flg==1)
                            
                                <i class="fas fa-crown crown-on"></i>
                            @else
                                <i class="fas fa-crown crown-off"></i>
                            @endif
                            </button>
                            <input type="hidden" name="hidden_answer_id" value="{{$forum_answer->id}}"/>
                        </form>
                        
                        <a class="btn btn-primary btn-sm" href="{{ route('answers.edit', ['answer' => $forum_answer])}}">
                            編集
                        </a>
                        

                        
                        <form
                            style="display: inline-block;"
                            method="POST"
                            action="{{ route('answers.destroy', ['answer' => $forum_answer]) }}"
                            >
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-sm">削除</button>
                        </form>
                    </div>
               @empty
                    <p>コメントはまだありません。</p>
                @endforelse
            </section>
        </div>
    </div>
@endsection