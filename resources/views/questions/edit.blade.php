@extends('layouts/layout')

@section('content')
    <div class="container mt-4">
        <div class="border p-4">
            <h1 class="h5 mb-4">
                質問の編集
            </h1>

            <form method="POST" action="{{ route('questions.update' , ['question' => $question]) }}">
                @csrf
                @method('PUT')

                <fieldset class="mb-4">
                    <div class="form-group">
                        <label for="title">
                            タイトル
                        </label>
                        <input
                            id="title"
                            name="title"
                            class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}"
                            value="{{ old('title') ?: $question->title }}"
                            type="text"
                        >
                        @if ($errors->has('title'))
                            <div class="invalid-feedback">
                                {{ $errors->first('title') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="question_comment">
                            本文
                        </label>

                        <textarea
                            id="question_comment"
                            name="question_comment"
                            class="form-control {{ $errors->has('question_comment') ? 'is-invalid' : '' }}"
                            rows="4"
                        >{{ old('question_comment') ?: $question->question_comment }}</textarea>
                        @if ($errors->has('question_comment'))
                            <div class="invalid-feedback">
                                {{ $errors->first('question_comment') }}
                            </div>
                        @endif
                    </div>

                    <div class="mt-5">
                        <a class="btn btn-secondary" href="{{ route('questions.show', ['question' => $question]) }}">
                            キャンセル
                        </a>

                        <button type="submit" class="btn btn-primary">
                            更新する
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@endsection