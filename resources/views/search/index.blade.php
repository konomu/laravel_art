@extends('layouts/app')

@section('content')
    <div class="container mt-4">
        検索ワード:{{ $keyword }}
        @forelse ($questions as $question)
            <div class="card mb-4">
                <div class="card-header">
                    {{ $question->title }}
                </div>
                <div class="card-body">
                    <p class="card-text">
                        {!! nl2br(e(str_limit($question->question_comment, 200))) !!}
                    </p>
                    <a class="card-link" href="{{ route('questions.show', ['question' => $question]) }}">
                        続きを読む
                    </a>
                </div>
                <div class="card-footer">
                    <span class="mr-2">
                        投稿日時 {{ $question->created_at->format('Y.m.d') }}
                    </span>

                    @if ($question->answers()->count())
                        <span class="badge badge-primary">
                            コメント {{ $question->answers()->count() }}件
                        </span>
                    @endif
                </div>
            </div>
        @empty
            <p>投稿がありません</p>
        @endforelse

@endsection