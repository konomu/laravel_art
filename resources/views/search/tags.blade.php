@extends('layouts/app')

@section('content')

    <div class="container mt-4">
        
        <div class="border p-4">
            <h1 class="h5 mb-4">
                タグ
            </h1>
            @forelse($tags as $tag)
            <a href="{{url('tag_question',$tag->id)}}">
                <h1 class="h5 mb-4">
                    {{ $tag->name }}
                </h1>
            </a>
            @empty
            @endforelse
        </div>
    </div>



@endsection