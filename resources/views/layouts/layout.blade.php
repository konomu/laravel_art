<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>Art</title>

    <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
</head>
<body>
    <header class="navbar navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{ url('') }}">
                Art
            </a>
            <form  class="form-inline" action="{{ url('search') }}" method="post">
                @csrf
                <input class="form-control mr-2" type="text" placeholder="Search" name="keyword">
                <button class="btn btn-outline-light">検索</button>
            </form>
        </div>
    </header>

    <div>
        @yield('content')
    </div>
</body>
</html>