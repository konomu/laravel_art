@extends('layouts/layout')

@section('content')
    <div class="container mt-4">
        <div class="border p-4">
            <h1 class="h5 mb-4">
                質問の編集
            </h1>

            <form method="POST" action="{{ route('answers.update' , ['answer' => $answer]) }}">
                @csrf
                @method('PUT')

                <fieldset class="mb-4">
                    <div class="form-group">
                        <label for="answers_comment">
                            本文
                        </label>

                        <textarea
                            id="answers_comment"
                            name="answers_comment"
                            class="form-control {{ $errors->has('answers_comment') ? 'is-invalid' : '' }}"
                            rows="4"
                        >{{ old('answers_comment') ?: $answer->answers_comment }}</textarea>
                        @if ($errors->has('answers_comment'))
                            <div class="invalid-feedback">
                                {{ $errors->first('answers_comment') }}
                            </div>
                        @endif
                    </div>

                    <div class="mt-5">
                        <a class="btn btn-secondary" href="{{ route('questions.show', ['answer' => $answer]) }}">
                            キャンセル
                        </a>

                        <button type="submit" class="btn btn-primary">
                            更新する
                        </button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@endsection