@extends('layouts/app')

@section('content')

    <div class="container mt-4">
        @auth
            <div id="userinfo_box">
                <div id="image_box">
                    <img id="user-icon" src="{{asset('storage/post_images/'.auth()->user()->image_url,true)}}"></img>
                </div>
                <div id="name_box">
                    <p>{{auth()->user()->name}}</p>
                </div>
            </div>
            <a href={{url('myquestions')}}>
                <p>My Questions</p>
            </a>
            <a href={{url('myanswers')}}>
                <p>My Ansewrs</p>
            </a>
        @endauth
        
        @guest
            <div id="userinfo_box">
                <div id="image_box">
                    <img id="user-icon" src="https://art-laravel.lolipop.io/storage/post_images/BuwtwhjxO0OKj7Zsn7EM8aOWp59NqlHkqNyeUa4D.jpeg"></img>
                </div>
                <div id="name_box">
                    <p>{{"guest user"}}</p>
                </div>
                
            <a href={{url('myquestions_guest')}}>
                <p>My Questions</p>
            </a>
            <a href={{url('myanswers_guest')}}>
                <p>My Ansewrs</p>
            </a>
                
            </div>
        @endguest
        
        {{--common--}}
            <a href={{url('tags')}}>
                <p>Tags</p>
            </a>
        
        <div class="mb-4">
            <a href="{{ route('questions.create') }}" class="btn btn-primary">
            投稿を新規作成する
            </a>
        </div>
        @forelse ($questions as $question)
            <div class="card mb-4">
                <div class="card-header">
                    <img class="list-icon" height="30px" src="{{asset('storage/post_images/'.$question->user->image_url,true)}}"></img>
                    <p>{{$question->user->name}}</p>
                    {{ $question->title }}
                </div>
                <div class="card-body">
                    <p class="card-text">
                        {!! nl2br(e(str_limit($question->question_comment, 200))) !!}
                    </p>
                    <a class="card-link" href="{{ route('questions.show', ['question' => $question]) }}">
                        続きを読む
                    </a>
                </div>
                <div class="card-footer">
                    <span class="mr-2">
                        投稿日時 {{ $question->created_at->format('Y.m.d') }}
                    </span>

                    @if ($question->answers()->count())
                        <span class="badge badge-primary">
                            コメント {{ $question->answers()->count() }}件
                        </span>
                    @endif
                </div>
            </div>
        @empty
            <p>投稿がありません</p>
        @endforelse
    </div>
    <div class="d-flex justify-content-center mb-5">
        {{ $questions->links() }}
    </div>
@endsection